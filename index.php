<?php
    session_start();
    include_once "Models/User.php";
    include_once "Classes/DbClass.php";
    include_once "Classes/MainClass.php";

    if (isset($_SESSION['logged_user']) && \Models\User::getUser()) {
        header('Location: /shop/cabinet.php');

    } else {
        ?>
        <html>
            <a href="login.php">Войти</a><br>
            <a href="register.php">Регистрация</a>
        </html>
        <?php
    }

?>




