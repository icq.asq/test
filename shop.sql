-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Сен 22 2020 г., 10:38
-- Версия сервера: 10.4.13-MariaDB
-- Версия PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `price`) VALUES
(1, 3, 300),
(2, 3, 400),
(3, 4, 100),
(4, 5, 600),
(5, 6, 600),
(6, 8, 500),
(7, 10, 100),
(8, 11, 123),
(9, 12, 345),
(10, 18, 234),
(11, 3, 234),
(12, 2, 45);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fio` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `login`, `password`, `fio`) VALUES
(2, 'icq.asq@gmail.com', 'test', '$2y$10$Zhj92Q0SPTkvhFrIMc4Pou3hQ0W/ZnD.Q1TSnIEp5.zsxZWtwGTV2', 'Асия'),
(3, 'icq.asq1@gmail.com', 'test1', '$2y$10$2/r/OVvnfzrTCI48BAT3huyIZBPKVX64R4Lvd8olwAaqq8apJggmG', 'Асия'),
(4, 'icq.asq@gmail.com', 'qwewe', '$2y$10$I0ZnhlRghCDjV4XFhbJ.2e0wwRPzwVSwnav5mzm8zZx28TsrZqGSW', 'erwttrert'),
(5, 'icq.asq11@gmail.com', 'qwewe1', '$2y$10$tvkAxSyv6OvJk/xtCIoa2emHXOkI4etuJIQQwqat0FTiaP2YXWS8y', 'erwttrert1'),
(6, 'icq.asq11@gmail.com', 'qwewe1', '$2y$10$M1fXgA8yJiY8YgwCsFaI5OACfFFiPn8JRawmUcMi2dXP9x6qCPnna', 'erwttrert1'),
(7, 'qweqw@we.fd', 'qeqe', '$2y$10$0UuOKIPB6d1kBE1uyvqRS.HutVJodTNqTPpg2duCy7QLg19H5pYXm', 'sfsf'),
(8, 'qweqw@we.fd', 'dfgdfg', '$2y$10$PP6QegWhh2iRALTPDT5qgOHdfXwceXT2yo7Jmmjrmj4w7vXGSS9F6', 'sdfsd'),
(9, 'icq.asq@gmail.com', 'sfsdf', '$2y$10$v1BNCWOVaae0i9p.BH2Y9eR5qLdfObATkc5FtUHecCCoOCU5gQ9qK', 'qwe'),
(10, 'icq.asq@gmail.com', 'sfsdf', '$2y$10$dwoIbX5JMIwQq3jD4.oXAeJ0ZYUhqT5t.LjVI506hOZynqarMtc1y', 'qwe'),
(11, 'icq.asq@gmail.com', 'sfsdf', '$2y$10$erOStuwjbEQdJyZ8EpfygeC4BTixDr8v4U9aIq7ESZ1/S9qNJxMlS', 'qwe'),
(12, 'icq.asq@gmail.com', 'sfsdf', '$2y$10$aKtX5er.oJ8VPxdg/TeoceScOxw/p4ADRucUYo.fNZUECS6zN8c4i', 'qwe'),
(13, 'icq.asq@gmail.com', 'sfsdf', '$2y$10$DbkViupHFdTHK7jsw6Ip7.5eZwZKugm04h96iaq2.8b1sRPCZSWeW', 'qwe'),
(14, 'icq.asq@gmail.com', 'sfsdf', '$2y$10$Ikert9Ht4Odg28Dvf4sNTeBxE0xR8y2RE2nWwodkCz1ifKjVS1LXa', 'qwe'),
(15, 'icq.asq@gmail.com', 'sfsdf', '$2y$10$2Q.Ua10hJwjuT/Aar2olleOKykhWAUR.Ea.P/kFzzefB4ZvynXVOq', 'qwe'),
(16, 'icq.asq@gmail.com', 'sfsdf', '$2y$10$GcgHIxKGj8thWgq5MUEjZOJTUgQCCECTAprHc3k1.Q4pUXdWpfetq', 'qwe'),
(17, 'icq.asq@gmail.com', 'sfsdf', '$2y$10$NdO9qgb0Jy6xgGaZKngJWu/nT/vVfpYR0BMuhTcKGc3H4MOS4u7SW', 'qwe'),
(18, 'icq.asq@gmail.com', 'sfsdf', '$2y$10$rpWQDLC18j1I.WXAKttx3u9Fr1VdhnQpKqxt5xZOatQiD.H4kekAi', 'qwe'),
(19, 'icq.asq@gmail.com', 'dsgsdfg', '$2y$10$TorpPVo8zFdYS7XERZD8ZOAYto4nzfuQAon4rllvMujOF5lXJmt0e', 'Асия'),
(20, 'qwe@qwe.ru', 'asya', '$2y$10$iSaJSx0pmelDbqhogxRtgOfx7yJVUICr9lMA1.5j1nniNVXPNRfwi', 'Асия Рахматулина'),
(21, 'icq.asq@gmail.com', 'qweqwe', '$2y$10$HuhI6ausu5R9QO4ZJtFceO12petQj49t9FAKHbrN4qjIXXI5yFtXq', 'assiya'),
(22, 'test@qwe.ru', 'testtest', '$2y$10$f6rjoMhjLNqDGxnTBHJ2yOPNT4YCcUnQ2skReRC6gCvrV3J5TztXe', 'assiya'),
(23, 'asd@asd.asd', 'picnic', '$2y$10$cjnj/Dee3skPUC6cfkwDqesJEkx/Uch9opbVqnkY1yl8eaLVZFGLK', 'asd'),
(24, 'asd@asd.asd', 'picnic', '$2y$10$4uSq9Q9fNer7mEGkxu/O/e.1R3fpSdTVMvP3Kf0isWpvCFWGhqpei', 'dsa'),
(25, 'qweqwe@qwe.qwe', 'qweqwe', '$2y$10$jT52yRy003.Pd2fi8Vp8oOA1oaQrtyl3i092q9w6DY/6smZL2Thmu', 'qwe');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
