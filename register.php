<?php
session_start();
require_once "Classes/RegisterClass.php";
include_once "Classes/AuthorizeClass.php";
include_once "Classes/MainClass.php";
include_once "Models/User.php";
include_once "Classes/DbClass.php";

    if (isset($_POST['sign_up'])) {

        $data = $_POST;
        $errors = [];

        if ($data['password'] != $data['confirm_password']) {
            $errors[] = "Пароль и повторный пароль не совпадают";
        }

        if (empty($errors)) {
            $user = new \Classes\RegisterClass($data);

            if ($user->register()) {
                header("Location: /shop/cabinet.php");
            } else
                header("Location: /shop/");;

        } else {
            echo "<div style='color: red;'>" . array_shift($errors) . "</div><hr>";
        }

    }

?>

<html>

<form action="register.php" method="post">
    <div style="display: inline-block; text-align: right;">
        <label>Логин</label><br>
        <label>Email</label><br>
        <label>Имя</label><br>
        <label>Пароль</label><br>
        <label>Повторный пароль</label><br>
    </div>
    <div style="display: inline-block;">
        <input type="text" name="login" required value=""><br>
        <input type="email" name="email" required value=""><br>
        <input type="text" name="name" required value=""><br>
        <input type="password" name="password" required value=""><br>
        <input type="password" name="confirm_password" required value=""><br>
    </div>

    <button type="submit" name="sign_up">Регистрация</button>
</form>

</html>