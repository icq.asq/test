<?php
namespace Classes;

class AuthorizeClass
{

    public static function login($data)
    {

        $sql = "SELECT * FROM users WHERE login = ?";
        $arr = [$data['login']];
        $db = new DbClass();
        $query = MainClass::makeQuery($db, $sql, $arr);
        $user = $query->fetch(\PDO::FETCH_OBJ);
        if ($user && password_verify($data['password'], $user->password)) {
            self::authorize($user->id);
            return true;
        } else
            return false;

    }

    public static function authorize($user_id)
    {
        $_SESSION['logged_user'] = $user_id;

    }

    public static function logout()
    {
        if (isset($_SESSION['logged_user']))
            unset($_SESSION['logged_user']);
        header('Location: /shop/');
    }

}