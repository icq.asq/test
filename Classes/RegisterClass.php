<?php
namespace Classes;
use Models\User;

class RegisterClass
{

    private $data;

    function __construct(array $data)
    {
        $this->data = $data;
    }

    public function register()
    {
        if ($this->checkEmail()) {

            if ($this->checkLogin()) {
                $newUser = new User($this->data);
                if ($user_id = $newUser->addUserDB()) {
                    AuthorizeClass::authorize($user_id);
                    return true;
                } else
                    return false;

            } else
                return false;

        } else {
            return false;
        }
    }

    private function checkLogin()
    {
        return preg_match('/^[a-z\d]{3,16}$/i', htmlspecialchars($this->data['login']));

    }

    private function checkEmail()
    {
        return filter_var(htmlspecialchars($this->data['email']), FILTER_VALIDATE_EMAIL);
    }


}


