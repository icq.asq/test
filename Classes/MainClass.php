<?php
namespace Classes;

class MainClass
{

    public static function makeQuery(DbClass $query, $sql, $array, $getId = false)
    {
        $query->connect();
        $stmt = $query->pdo->prepare($sql);
        $stmt->execute($array);

        if ($stmt && $getId)
            return $query->pdo->lastInsertId();

        return $stmt;

    }

    public static function getData($type)
    {
        $sql = "";
        if ($type == "emails")
            $sql = "SELECT DISTINCT(email) FROM users AS t1 WHERE 
                    (SELECT COUNT(email) FROM users AS t2 WHERE t2.email=t1.email) > 1";
        if ($type == "login")
            $sql = "SELECT login FROM users WHERE id NOT IN (SELECT user_id FROM orders)";
        if ($type == "login2")
            $sql = "SELECT login FROM users WHERE (SELECT COUNT(user_id) FROM orders WHERE user_id = users.id) > 2";

        $array = [];
        $db = new \Classes\DbClass();
        $query = self::makeQuery($db, $sql, $array);
        return $query->fetchAll(\PDO::FETCH_COLUMN, 0);

    }

}