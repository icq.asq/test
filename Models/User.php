<?php
namespace Models;

use Classes\MainClass;
use Classes\DbClass;

class User
{
    private $data;
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function addUserDB()
    {

        $sql = "INSERT INTO users (email, login, password, fio) VALUES (?,?,?,?)";
        $arr = [$this->data['email'], $this->data['login'],
                password_hash($this->data['password'], PASSWORD_DEFAULT), $this->data['name']];

        $db = new DbClass();
        $new_user_id = MainClass::makeQuery($db, $sql, $arr, true);

        return $new_user_id ? $new_user_id : false;

    }

    public static function getUser()
    {

        if (isset($_SESSION['logged_user'])) {
            $db = new DbClass;
            $sql = "SELECT * FROM users WHERE id = ? LIMIT 1";
            $arr = [$_SESSION['logged_user']];
            $users = MainClass::makeQuery($db, $sql, $arr);

            $user = $users->fetch(\PDO::FETCH_OBJ);
            return $user;
        } else
            return false;

    }

    public static function changeUserData($data)
    {
        $user_id = $_SESSION['logged_user'];

        if ($data['name'] != "") {
            $sql = "UPDATE users SET fio = ? WHERE id = ?";
            $array = [$data['name'], $user_id];
        }

        if ($data['password'] != "" && $data['password'] == $data['confirm_password']) {
            $sql = "UPDATE users SET password = ? WHERE id = ?";
            $array = [password_hash($data['password'], PASSWORD_DEFAULT), $user_id];
        }

        if (!empty($sql) && !empty($array)){
            $db = new DbClass();
            MainClass::makeQuery($db, $sql, $array);
        }


        header('Location: /shop/cabinet.php');

    }

}