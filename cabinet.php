<?php
    session_start();
    require_once "Models/User.php";
    require_once "Classes/MainClass.php";
    require_once "Classes/DbClass.php";

    $user = Models\User::getUser();

    $emails = Classes\MainClass::getData("emails");
    $login = Classes\MainClass::getData("login");
    $login2 = Classes\MainClass::getData("login2");
?>

<html>

    <div style="display: inline-block; text-align: right;">
        <label>Логин: </label><br>
        <label>Email: </label><br>
        <label>Имя: </label><br>
    </div>
    <div style="display: inline-block;">
        <label> <?php echo $user->login?></label><br>
        <label> <?php echo $user->email?></label><br>
        <label> <?php echo $user->fio?></label><br>
    </div>

    <form action="main.php" method="post">

        <h4>Изменить имя и пароль</h4>

        <div style="display: inline-block; text-align: right">
            <label>Имя: </label><br>
            <label>Пароль: </label><br>
            <label>Повторный пароль: </label><br>
        </div>
        <div style="display: inline-block;">
            <input type="text" name="name"><br>
            <input type="password" name="password"><br>
            <input type="password" name="confirm_password"><br>
        </div>
        <button type="submit" name="save">Сохранить</button>
    </form>

    <form method="post" action="main.php">
        <button name="logout" type="submit">Выход</button>
    </form>

    <h4>Повторяющиеся email:</h4>
    <?php
        foreach ($emails as $k=>$e)
        {
            echo ++$k.". ".$e."<br>";
        }
    ?>

    <h4>Список пользователей, которые не сделали ни одного заказа:</h4>
    <?php
    foreach ($login as $k=>$l)
    {
        echo ++$k.". ".$l."<br>";
    }
    ?>

    <h4>Список пользователей которые сделали более двух заказов:</h4>
    <?php
    foreach ($login2 as $k=>$l)
    {
        echo ++$k.". ".$l."<br>";
    }
    ?>

</html>
