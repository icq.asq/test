<?php
session_start();

include_once "Classes/AuthorizeClass.php";
include_once "Classes/MainClass.php";
include_once "Models/User.php";
include_once "Classes/DbClass.php";

if (isset($_POST['do_login'])) {

    $data = $_POST;
    $result = \Classes\AuthorizeClass::login($data);

    if ($result)
        header('Location: /shop/cabinet.php');
    else header('Location: /shop/');

}

if (isset($_POST['logout'])) {
    \Classes\AuthorizeClass::logout();
}

if (isset($_POST['save'])) {
    \Models\User::changeUserData($_POST);
}

